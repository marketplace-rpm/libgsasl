# Information / Информация

SPEC-файл для создания RPM-пакета **libgsasl**.

## Install / Установка

1. Подключить репозиторий **MARKETPLACE**: `dnf copr enable marketplace/libs`.
2. Установить пакет: `dnf install libgsasl`.

## Donation / Пожертвование

- [Donating](https://donating.gitlab.io/)